
%                            --------------------
%                            FONTS AND CHARACTERS
%                            --------------------


%   FONTS
%
\font\sevenit=cmti10 scaled 700
\font\eightrm=cmr8
\font\ninerm=cmr9
\font\title=cmbx12 scaled 1400
\font\authors=cmbx12


%   SPECIAL CHARACTER MACROS
%
\def\leaderfill{\leaders\hbox to 0.3em{\hss.\hss}\hfill}
\def\bull{\item{$\bullet$}}
\def\dash{\hbox{--}}
\def\degree{{$^\circ$}}
\def\n#1{$^{#1}$}
\def\pmb#1{\setbox0=\hbox{#1}%
  \kern-.001em\copy0\kern-\wd0
  \kern-.02em\copy0\kern-\wd0
  \kern-.015em\copy0\kern-\wd0
  \kern-.01em\copy0\kern-\wd0
  \kern-.005em\copy0\kern-\wd0
  \kern.005em\copy0\kern-\wd0
  \kern.01em\copy0\kern-\wd0
  \kern.015em\copy0\kern-\wd0
  \kern.02em\copy0\kern-\wd0
  \kern-.02em\raise.02em\box0 } 
\def\ts{\thinspace}           % binding thinspace


%   CHECK FOLLOWING CHARACTER
%
%   After some macros, one would like to put a space if not followed by
%   punctuation.  "\SP" will check the following character--if it is a
%   not closing punctuation, a space is inserted.
%
\def\puncspace{\ifmmode\,\else{\ifcat.\C{\if.\C\else\if,\C\else\if?\C\else%
\if:\C\else\if;\C\else\if-\C\else\if)\C\else\if/\C\else\if]\C\else\if'\C%
\else\space\fi\fi\fi\fi\fi\fi\fi\fi\fi\fi}%
\else\if\empty\C\else\if\space\C\else\space\fi\fi\fi}\fi}
\def\SP{\let\\=\empty\futurelet\C\puncspace}


%   HYPHENATION EXCEPTIONS
%
\hyphenation{where-as wheth-er phys-ics Sha-piro
hydro-dynamical}


%   MACROS USABLE IN OR OUT OF MATH MODE
%
\def\x{\ifmmode {} \times {} \else ${} \times {}$\fi} 
\def\ee#1{\ifmmode {} \times 10^{#1} \else ${} \times 10^{#1}$\fi} 
\def\about{\ifmmode \sim \else {$\sim$\thinspace}\fi}
\def\aboutless{\ifmmode {\mathbin{\lower 3pt\hbox
    {$\rlap{\raise 5pt\hbox{$\char'074$}}\mathchar"7218$}}} {}  %< or of order
    \else {${\mathbin{\lower 3pt\hbox
    {$\rlap{\raise 5pt\hbox{$\char'074$}}\mathchar"7218$}}}   %< or of order
    $\thinspace}\fi}
\def\aboutmore{\ifmmode {\mathbin{\lower 3pt\hbox
    {$\rlap{\raise 5pt\hbox{$\char'076$}}\mathchar"7218$}}} {}  %> or of order
    \else {${\mathbin{\lower 3pt\hbox
    {$\rlap{\raise 5pt\hbox{$\char'076$}}\mathchar"7218$}}}   %> or of order
    $\thinspace}\fi}


%   MACROS FOR USE IN TEXT MODE
%
%   (e.g., "for a mass \aboutless 2\ee{-9} g")
%
\def\sub#1{\ifmmode _{#1} \else $_{#1}$\fi}
\def\sup#1{\ifmmode ^{#1} \else $^{#1}$\fi}


%   MACROS FOR USE IN MATH MODE ONLY
%
%   (e.g., "since $M \simless 2\ee{-9}$ g")
%
\def\ast{\mathchar"2203} \mathcode`*="002A   % makes * ok in math
\def\simless{\mathbin{\lower 3pt\hbox
     {$\rlap{\raise 5pt\hbox{$\char'074$}}\mathchar"7218$}}}   %< or of order
\def\simgreat{\mathbin{\lower 3pt\hbox
     {$\rlap{\raise 5pt\hbox{$\char'076$}}\mathchar"7218$}}}   %> or of order
\def\del{{\nabla}}
\def\div{{\nabla\cdot}}
\def\pd#1{{\partial\displaystyle #1}}   % abbreviation for partials; e.g. \pd{t}
\def\comma{{\;\;,}}
\def\period{{\;\;.}}



%                         -------------------------------
%                         NAMES, UNITS, AND ABBREVIATIONS
%                         -------------------------------



%   COMMON ITALICIZED ABBREVIATIONS
%
\def\eg{{\it e.\thinspace g.}\SP}
\def\ibid{{\it ibid}\SP}
\def\ie{{\it i.\thinspace e.}\SP}
\def\etal{{\it et al.}\SP}
\def\cf{{\it cf.}\SP}
\def\loccit{{\it loc. cit.}\SP}
\def\opcit{{\it op. cit.}\SP}


%   UNITS
%
\def\cm{{\hbox{cm}}\SP}
\def\cmps{{\hbox{cm\ts s\sup{-1}}}\SP}
\def\eps{{\hbox{ergs\ts s\sup{-1}}}\SP}
\def\ergps{{\hbox{ergs\ts s\sup{-1}}}\SP}
\def\epsqcm{{\hbox{ergs\ts cm\sup{-2}}}\SP}
\def\epsqcmps{{\hbox{ergs\ts cm\sup{-2}\ts s\sup{-1}}}\SP}
\def\gcmsq{{\hbox{g\ts cm\sup2}}\SP}
\def\gpsqcm{{\hbox{g\ts cm\sup{-2}}}\SP}
\def\gcmrps{{\hbox{g\ts cm\sup2\ts rad\ts s\sup{-1}}}\SP}
\def\gpcc{{\hbox{g\ts cm\sup{-3}}}\SP}
\def\gps{{\hbox{g\ts s\sup{-1}}}\SP}
\def\Gpcc{{\hbox{G\ts cm\sup{-3}}}\SP}
\def\keV{{\hbox{keV}}\SP}
\def\kps{{\hbox{km\ts s\sup{-1}}}\SP}
\def\Msunpy{{\hbox{\Msun\ts yr\sup{-1}}}\SP}
\def\pcc{{\hbox{cm\sup{-3}}}\SP}
\def\ps{{\hbox{s\sup{-1}}}\SP}
\def\psqcm{{\hbox{cm\sup{-2}}}\SP}
\def\rps{{\hbox{rad\ts s\sup{-1}}}\SP}
\def\rpss{{\hbox{rad\ts s\sup{-2}}}\SP}
\def\statcpcc{{\hbox{statcoulomb\ts cm\sup{-3}}}\SP}
\def\statcpsqc{{\hbox{statcoulomb\ts cm\sup{-2}}}\SP}
\def\V{{\hbox{Volts}}\SP}


%   SOURCES
%
\def\a#1{\leavevmode\hbox{A~#1}\SP}
\def\aql#1{\leavevmode\hbox{Aql~X-#1}\SP}
\def\cen#1{\leavevmode\hbox{Cen~X-#1}\SP}
\def\cir#1{\leavevmode\hbox{Cir~X-#1}\SP}
\def\cyg#1{\leavevmode\hbox{Cyg~X-#1}\SP} 
\def\fu#1{\leavevmode\hbox{4U~#1}\SP}
\def\gro#1{\leavevmode\hbox{GRO~#1}\SP} 
\def\gx#1{\leavevmode\hbox{GX~#1}\SP} 
\def\her#1{\leavevmode\hbox{Her~X-#1}\SP} 
\def\ks#1{\leavevmode\hbox{KS~#1}\SP}
\def\lmc#1{\leavevmode\hbox{LMC~X-#1}\SP}
\def\psr#1{\leavevmode\hbox{PSR~#1}\SP} 
\def\sco#1{\leavevmode\hbox{Sco~X-#1}\SP} 
\def\smc#1{\leavevmode\hbox{SMC~X-#1}\SP}
\def\fu#1{\leavevmode\hbox{4U~#1}\SP} 
\def\vela#1{\leavevmode\hbox{Vela~X-#1}\SP} 
\def\x#1{\leavevmode\hbox{X~#1}\SP}
\def\xte#1{\leavevmode{XTE~#1}\SP}

%   SATELLITES

\def\axaf{\leavevmode{Chandra\/}\SP}
\def\batse{\leavevmode{BATSE\/}\SP}
\def\chandra{\leavevmode{Chandra\/}\SP}
\def\conx{\leavevmode{Constellation-X\/}\SP}
\def\einstein{\leavevmode{\it Einstein Observatory\/}\SP}
\def\exosat{\leavevmode{EXOSAT\/}\SP}
\def\ginga{\leavevmode{\it Ginga\/}\SP}
\def\compton{\leavevmode{\it Compton Gamma-Ray Observatory\/}\SP}
\def\hakucho{\leavevmode{\it Hakucho\/}\SP}
\def\heao#1{\leavevmode{HEAO~#1\/}\SP}
\def\oso#1{\leavevmode{OSO~#1\/}\SP}
\def\rosat{\leavevmode{ROSAT\/}\SP}
\def\rxte{\leavevmode{\it RXTE\/}\SP}
\def\sax{\leavevmode{SAX\/}\SP}
\def\spacetelescope{\leavevmode{\it Hubble Space Telescope\/}\SP}
\def\tenma{\leavevmode{\it Tenma\/}\SP}
\def\uhuru{\leavevmode{\it Uhuru\/}\SP}
\def\xmm{\leavevmode{\it XMM\/}\SP}


%                           ------------------------
%                           SECTIONS AND SUBSECTIONS
%                           ------------------------


\newskip\preskipamount
  \preskipamount=6pt plus2pt minus2pt
\newskip\postskipamount
  \postskipamount=4pt plus1pt minus1pt
\def\preskip{\vskip\preskipamount\nobreak}
\def\postskip{\vglue\postskipamount\nobreak}
%
\def\abstract{\preskip\preskip\centerline{\bf ABSTRACT}\postskip\postskip}
%
\newcount\secno
\newcount\subsecno
\newcount\subsubsecno
%\def\clrsecno{\global\secno=0} \clrsecno
\secno=0
\subsecno=0
\subsubsecno=0
%\def\secnum{}
%
\def\section#1
  {\advance\secno by1 \subsecno=0
  {\global\eqnumber=0}
  {\global\tablenumber=0}
  {\global\fignumber=0}
%  \def\secnum{\the\secno}
%  \preskip\centerline{\bf\secnum.~~#1}\postskip}
  \def\secnum{\the\secno}
  \preskip\centerline{\bf#1}\postskip}
%
\def\subsection#1
  {\advance\subsecno by1 \subsubsecno=0
%  {\global\eqnumber=0}
%  {\global\tablenumber=0}
%  {\global\fignumber=0}
  \def\subsecnum{\the\subsecno}
%  \preskip\centerline{\bf\secnum.\subsecnum~~#1}\postskip}
  \preskip\centerline{\bf#1}\postskip}
%
\def\firstsubsection#1
  {\advance\subsecno by1 \subsubsecno=0
%  {\global\eqnumber=0}
%  {\global\tablenumber=0}
%  {\global\fignumber=0}
%  \def\subsecnum{\the\subsecno}
  \centerline{\bf#1}\postskip}
%
\def\subsubsection#1
  {\advance\subsubsecno by1 
%  {\global\eqnumber=0}
%  {\global\tablenumber=0}
%  {\global\fignumber=0}
  \def\subsubsecnum{\the\subsubsecno}
  \preskip{\noindent\bf#1}\postskip}
%
\def\firstsubsubsection#1
  {\advance\subsubsecno by1 
%  {\global\eqnumber=0}
%  {\global\tablenumber=0}
%  {\global\fignumber=0}
  \def\subsubsecnum{\the\subsubsecno}
  {\noindent\bf#1}\postskip}
%
\def\acknowledgments{\preskip\centerline{\bf Acknowledgments}\postskip}


%                          ------------------
%                          PAGE LAYOUT MACROS
%                          ------------------


%   NEW LINE AND NEW PAGE MACROS
%
\def\newline{\hfil\break} %   fill current line and start new one
\def\newpage{\vfill\eject}%   fill current page and start new one


%   VERTICAL SKIP MACROS
%
%   vertical skips which will not disappear at the top of a page
%   (as opposed to \smallskip, etc.)
%
\def\SMALLskip{\vglue\smallskipamount\nobreak}
\def\MEDskip{\vglue\medskipamount\nobreak}
\def\BIGskip{\vglue\bigskipamount\nobreak}


%   SPECIAL CENTERING MACRO
%
%   \center will horizontally center all text that is passed to it,  and
%   will break it up in to different lines if it is too long to fit into
%   a single line.  A forced line break can be made with "\\".
%
\def\center#1{{\def\\{\break}\rightskip=0pt plus1fil \leftskip=\rightskip
     \parindent=0pt \parfillskip=0pt #1\par}}


%   EQUATION SPACING
%
%   redefines spacing for displayed equations...
%   redefine \jot (specifies the amount of extra vertical
%   space in equations in display mode)
%
\jot=16pt
\abovedisplayskip=10pt plus 3pt minus 6pt
\abovedisplayshortskip=0pt plus 3pt
\belowdisplayskip=10pt plus 3pt minus 6pt
\belowdisplayshortskip=12pt plus 3pt minus 6pt


%   REFERENCES
%
%   Usage: \references ... \endreferences
%      Skip lines between each reference.
%      Use \bysame for blankline (used for repeated author listings).

%  \def\references{\vskip0pt plus.05\vsize\begingroup
%  \frenchspacing\raggedright
%  \parskip0pt\baselineskip=12pt\let\everypar=\filbreak
%  \parskip0pt\baselineskip=24pt\let\everypar=\filbreak
%  \def\par{\endgraf\hangindent12pt\hangafter=1} \parindent0pt \vskip0pt}

\def\references{\vskip0pt plus.05\vsize\begingroup
    \frenchspacing\raggedright}
\def\endreferences{\parindent=20pt\endgroup}
\def\bysame{\vrule height-1pt depth1.5pt width 40pt .\ }

%  Reference macros: To generate reference to a paper by the Easter Bunny
%  in Ap.J. vol 2010, 789, write \ref{title}{E. Bunny}{\apj}{2010}{789}{1989}

%  \def\refindent{\par\penalty-100\noindent\parskip=4pt plus1pt
%               \hangindent=3pc\hangafter=1\null}
 \def\refindent{\par\penalty-100\noindent
               \hangindent 20pt\hangafter=1\null}

 \newcount\refno
 \def\ref#1#2#3#4#5#6{{\global\advance\refno
by1\item{\the\refno.}{#1. {\it #3,\ }{\bf#4},
#5 (#6). #2.}\medskip}}
% \def\ref#1#2#3#4#5#6{{\global\advance\refno
%by1\item{\the\refno.}{#2. #1. {\it #3,\ }{\bf#4},
%#5 (#6).}\medskip}}
 \def\refsub#1#2#3{{\global\advance\refno by
1\item{\the\refno.}{#2. #1.{\it ~#3,\ }
submitted.}\medskip}}
 \def\refpress#1#2#3{{\global\advance\refno by
1\item{\the\refno.}{#2. #1.{\it ~#3,\ } in
press.}\medskip}}
 \def\refbook#1#2#3{{\global\advance\refno by
1\item{\the\refno.}{#1. #3. #2.}\medskip}}
% \def\refbook#1#2#3{{\global\advance\refno by
%1\item{\the\refno.}{#2. #1. #3.}\medskip}}
 \def\refiauc#1#2#3#4{{\global\advance\refno by
1\item{\the\refno.}{#2. #1. {\it IAU Circular},
No.~#3 (#4).}\medskip}}

%\def\refnum#1{\global\advance\refno by1\item{\the\refno.}{#1}\par}
%\def\refnum#1{{\global\advance\refno by1}\item{\the\refno.}{#1}\medskip}

% Define journal abbreviations
  \def\aa{{Astr.\ Astro\-phys.}}
  \def\aat{{Astr.\ Astro\-phys.\ Transact.}}
  \def\aj{{Astr.\ J.}}
  \def\annrev{{Ann.\ Rev.\ Astr.\ Astro\-phys.}}
  \def\anny{{Ann.\ New York Acad.\ Sci.}}
  \def\apj{{Astro\-phys.\ J.}}
  \def\apjs{{Astro\-phys.\ J.\ Suppl.}}
  \def\apjl{{Astro\-phys.\ J.\ (Letters)}}
  \def\aplc{{Astro\-phys.\ Lett.\ \& Comm.}}
  \def\mnras{{Mon.\ Not.\ R.\ Astr.\ Soc.}}
  \def\npa{{Nucl.\ Phys. A}}
  \def\npb{{Nucl.\ Phys. B}}
  \def\pasp{{Publ.\ astr.\ Soc.\ Pacif.}}
  \def\pl{{Phys.\ Lett.}}
  \def\prd{{Phys.\ Rev.\ D.}}
  \def\prl{{Phys.\ Rev.\ Lett.}}
  \def\rmp{{Rev.\ Mod.\ Phys.}}
  \def\and{{and~}}


%   BIO REFERENCES
%
%   Usage: \bioreferences ... \endbioreferences
%      Skip lines between each reference.
%      Use \bysame for blankline (used for repeated author listings).
\def\bioreferences{\vskip0pt plus.05\vsize\begingroup
  \frenchspacing\raggedright
  \parskip0pt\baselineskip=10pt\let\everypar=\filbreak
  \def\par{\endgraf\hangindent12pt\hangafter=1} \parindent0pt \vskip0pt}
\def\endbioreferences{\parindent=20pt\endgroup}
\def\bysame{\vrule height-1pt depth1.5pt width 40pt .\ }


%   FIGURES
%
\def\picture #1 by #2 (#3){
  \vbox to #2{
    \hrule width #1 height 0pt depth 0pt
    \vfill
    \special{picture #3} % this is the low-level interface
    }
  }
\def\scaledpicture #1 by #2 (#3 scaled #4){{
  \dimen0=#1 \dimen1=#2
  \divide\dimen0 by 1000 \multiply\dimen0 by #4
  \divide\dimen1 by 1000 \multiply\dimen1 by #4
  \picture \dimen0 by \dimen1 (#3 scaled #4)}
  }


%   CAPTIONS
%
\def\caption#1#2{\vbox{\noindent\ninerm\baselineskip=10pt Fig.~{#1}---{#2}}} 



%                ------------------------------------------------
%                EQUATION, TABLE, AND FIGURE NAMING AND NUMBERING
%                ------------------------------------------------


%   EQUATION NUMBERING
%
%   \eqn and \eqnalign perform automatic numbering of displayed equations.
%   \eqn should be placed at the end of the displayed equation,
%   immediately preceding the closing $$. For example, 
%                $$a=b+c\eqn$$
%   will generate an equation like:
%               a = b + c                               (2.17)
%   where 2 is the chapter number (if any) followed by . and the sequential
%   number of the equation in the chapter. \eqnalign does the same thing
%   when you are using \eqalignno to layout an equation of several lines.
%
%   If the equation number is referenced later in the text, an
%   equation name should be placed after \eqn, e.g.
%     $$ a^2=b^2+c^2 \eqn\Pythag$$... From equation \Pythag we see...
%
%   Appending an a, b, or c to an equation number is accomplished
%   by use of \eqna, \eqnb, or \eqnc, if the equation is an ordinary one, 
%   or \eqnaligna, \eqnalignb, or \eqnalignc, if \eqalignno is being used. 
%   These macros will maintain proper numerical sequencing
%   (e.g. 16,17a,17b,18,19).
%    
%   \clreqnumber resets the numbers to zero.
%   \lasteqn prints the number of the most recently printed equation.
%   \nexteqn prints the number of the next equation that would be printed.
%
\newcount\eqnumber
\def\clreqnumber{\global\eqnumber=0} \clreqnumber
%
\def\EQN#1#2$${\global\advance\eqnumber by1%
  \eqno\hbox{\rm(\secnum.\the\eqnumber#1)}$$\def\name{#2}\ifx\name\empty%
  \else\xdef#2{\secnum.\the\eqnumber#1\noexpand\SP}\fi\ignorespaces}
%  \eqno\hbox{\rm(\the\eqnumber#1)}$$\def\name{#2}\ifx\name\empty%
%  \else\xdef#2{\the\eqnumber#1\noexpand\SP}\fi\ignorespaces}
\def\EQNALIGN#1#2{\global\advance\eqnumber by1%
  \hbox{\rm(\secnum.\the\eqnumber#1)}\def\name{#2}\ifx\name\empty%
  \else\xdef#2{\secnum.\the\eqnumber}\fi\ignorespaces}
%
\def\eqn#1$${\EQN{}{#1}$$}
\def\eqna#1$${\EQN{a}{#1}$$}
\def\eqnb#1$${\global\advance\eqnumber by-1 \EQN{b}{#1}$$}
\def\eqnc#1$${\global\advance\eqnumber by-1 \EQN{c}{#1}$$}
\def\eqnalign#1{\EQNALIGN{}{#1}}
\def\eqnaligna#1{\EQNALIGN{a}{#1}}
\def\eqnalignb#1{\global\advance\eqnumber by-1 \EQNALIGN{b}{#1}}
\def\eqnalignc#1{\global\advance\eqnumber by-1 \EQNALIGN{c}{#1}}
\def\lasteqn{{\rm\secnum.\the\eqnumber}\SP}
\def\nexteqn{\advance\eqnumber by1 {\rm\secnum.\the\eqnumber}\advance 
  \eqnumber by-1 \SP}



%   TABLE NAMING AND NUMBERING
%
%   \table, when inserted at the first reference to a table, performs automatic  
%   numbering of tables. The name of the table should be placed immediately  
%   after \table, e.g., "...as shown in Table~\table\Masses." The table can then 
%   be referred to at any later point by name, e.g., "The masses listed in
%   Table~\MassDist show that..."
%
\newcount\tablenumber
\def\clrtablenumber{\global\tablenumber=0} \clrtablenumber
\def\TABLE#1#2{\global\advance\tablenumber by1%
  \hbox{\rm\secnum.\the\tablenumber#1}\def\name{#2}\ifx\name\empty%
  \else\xdef#2{\secnum.\the\tablenumber#1}\fi\ignorespaces}
\def\table#1{\TABLE{}{#1}}
\def\lasttable{{\rm\secnum.\the\tablenumber}\SP}
\def\nexttable{\advance\tablenumber by1 {\rm\secnum.\the\tablenumber}\advance
  \tablenumber by-1 \SP}


%   FIGURE NUMBERING
%
%   \fig, when inserted at the first reference to a figure, performs automatic  
%   numbering of figures. The name of the figure should be placed immediately  
%   after \fig, e.g., "...as shown in Figure~\fig\MassDist." The figure can then 
%   be referred to at any later point by name, e.g., "The mass distribution 
%   shown in Figure~\MassDist indicates that..."
%
\newcount\fignumber
\def\clrfignumber{\global\fignumber=0} \clrfignumber
\def\FIG#1#2{\global\advance\fignumber by1%
  \hbox{\rm\secnum.\the\fignumber#1}\def\name{#2}\ifx\name\empty%
  \else\xdef#2{\rm\secnum.\the\fignumber#1\noexpand\SP}\fi\ignorespaces}
\def\fig#1{\FIG{}{#1}}
\def\lastfig{{\chapno\the\fignumber}\SP}
\def\nextfig{\advance\fignumber by1 {\chapno\the\fignumber}\advance
  \fignumber by-1 \SP}



%                             ----------
%                             TIME STAMP
%                             ----------


\def\monthname{%
  \ifcase\month
     \or Jan\or Feb\or Mar\or Apr\or May\or Jun%
     \or Jul\or Aug\or Sep\or Oct\or Nov\or Dec%
  \fi}%

\def\timestring{\begingroup
  \count0 = \time \divide\count0 by 60
  \count2 = \count0   % The hour.
  \count4 = \time \multiply\count0 by 60
  \advance\count4 by -\count0   % The minute.
  \ifnum\count4<10 \toks1 = {0}% Get a leading zero.
  \else            \toks1 = {}%
  \fi
  \ifnum\count2<12 \toks0 = {a.m.}%
  \else            \toks0 = {p.m.}%
     \advance\count2 by -12
  \fi
  \ifnum\count2=0 \count2 = 12 \fi % Make midnight '12'.
  \number\count2:\the\toks1 \number\count4
  \thinspace \the\toks0
\endgroup}%

\def\timestamp{\number\year\space\monthname\space
  \number\day\quad\timestring}%



%                          -----------------
%                          HEADERS & FOOTERS
%                          -----------------

%\headline={\hfil}
%\footline={\hfil}
%\headline={\ifnum\pageno=0{\hfil}\else{\hss\tenrm\folio\hss}\fi}
%\headline={\ifnum\pageno<2{\hfil} \else {\headlinefill}\fi}
\headline={\headlinefill}
\def\headlinefill{\ifodd\pageno\oddheadline\else\evenheadline\fi}
%\def\oddheadline{\hss STUDIES IN THEORETICAL PHYSICS AND ASTROPHYSICS\hss}
%\def\evenheadline{\hss LAMB, PETHICK, AND SHAPIRO \hss}
%
\footline={\ifnum\pageno<1{\hfil}
  \else{\hss\dash~\tenrm\folio~\dash\hss}\fi}


%             ------------------------------------------
%             USEFUL MATH SYMBOLS SPECIFIC TO THIS PAPER
%             ------------------------------------------


\def\fosc{\ifmmode f_{\rm osc} \else {$f_{\rm osc}$}\fi}
\def\LE{\ifmmode L_E \else {$L_E$}\fi}
\def\Lsun{\ifmmode L_\odot \else {$L_{\odot}$}\fi}
\def\Mdot{\ifmmode \dot M \else {$\dot M$}\fi}
\def\Mdotd{\ifmmode \dot M_d \else {$\dot M_d$}\fi}
\def\MdotE{\ifmmode \dot M_E \else {$\dot M_E$}\fi}
\def\Mdotr{\ifmmode \dot M_r \else {$\dot M_r$}\fi}
\def\Msun{\ifmmode M_\odot \else {$M_{\odot}$}\fi}
\def\sun{\ifmmode _\odot \else {$_{\odot}$}\fi}
\def\tff{\ifmmode \tau_{\ts\rm f-f} \else {$\tau_{\ts\rm f-f}$}\fi}  
\def\tesc{\ifmmode t_{\rm esc} \else {$t_{\rm esc}$}\fi}
\def\Teff{\ifmmode T_{\rm eff} \else {$T_{\rm eff}$}\fi}
\def\Tff{\ifmmode T_{\rm f\hbox{}f} \else {$T_{\rm f\hbox{}f}$}\fi}

\def\rem#1{{\bf#1}}


%   MACROS USABLE IN OR OUT OF MATH MODE

\def\ee#1{\ifmmode {} \times 10^{#1} \else ${} \times 10^{#1}$\fi}
\def\sub#1{\ifmmode _{#1} \else $_{#1}$\fi}
\def\sup#1{\ifmmode ^{#1} \else $^{#1}$\fi}

\def\dash{\hbox{--}}
\def\about{\ifmmode \sim \else {$\sim\,$}\fi}
\def\lta{\ifmmode {\,\mathbin{\lower 3pt\hbox   %< or of order
    {$\,\rlap{\raise 5pt\hbox{$\char'074$}}\mathchar"7218\,$}}}
    \else {${\mathbin{\lower 3pt\hbox
    {$\rlap{\raise 5pt\hbox{$\char'074$}}\mathchar"7218\,$}}}
    $}\fi}
\def\gta{\ifmmode {\mathbin{\lower 3pt\hbox   %> or of order
    {$\,\rlap{\raise 5pt\hbox{$\char'076$}}\mathchar"7218\,$}}}
    \else {${\mathbin{\lower 3pt\hbox
    {$\rlap{\raise 5pt\hbox{$\char'076$}}\mathchar"7218\,$}}}
    $}\fi}

%   MACROS FOR USE IN MATH MODE ONLY

\def\ast{\mathchar"2203} \mathcode`*="002A   % makes * ok in math
\def\del{{\nabla}}
\def\div{{\nabla\cdot}}
\def\pd#1{{\partial\displaystyle #1}} %abbreviation for partials; \pd{t}
\def\comma{{\;\;,}}
\def\period{{\;\;.}}

%           ------------------------------
%           UNITS, SOURCES, AND SATELLITES
%           ------------------------------

%   UNITS

\def\cm{{\hbox{cm}}\SP}
\def\cmps{{\hbox{cm\ts s\sup{-1}}}\SP}
\def\degree{{\ifmmode ^\circ \else $^\circ$\fi}}
\def\eps{{\hbox{ergs\ts s\sup{-1}}}\SP}
\def\ergps{{\hbox{ergs\ts s\sup{-1}}}\SP}
\def\epsqcm{{\hbox{ergs\ts cm\sup{-2}}}\SP}
\def\epsqcmps{{\hbox{ergs\ts cm\sup{-2}\ts s\sup{-1}}}\SP}
\def\gcmsq{{\hbox{g\ts cm\sup2}}\SP}
\def\gpsqcm{{\hbox{g\ts cm\sup{-2}}}\SP}
\def\gcmrps{{\hbox{g\ts cm\sup2\ts rad\ts s\sup{-1}}}\SP}
\def\gpcc{{\hbox{g\ts cm\sup{-3}}}\SP}
\def\gps{{\hbox{g\ts s\sup{-1}}}\SP}
\def\Gpcc{{\hbox{G\ts cm\sup{-3}}}\SP}
\def\Hz{{\hbox{Hz}}\SP}
\def\keV{{\hbox{keV}}\SP}
\def\km{{\hbox{km}}\SP}
\def\kps{{\hbox{km\ts s\sup{-1}}}\SP}
\def\msunpy{{\hbox{\msun\ts yr\sup{-1}}}\SP}
\def\pcc{{\hbox{cm\sup{-3}}}\SP}
\def\ps{{\hbox{s\sup{-1}}}\SP}
\def\psqcm{{\hbox{cm\sup{-2}}}\SP}
\def\rps{{\hbox{rad\ts s\sup{-1}}}\SP}
\def\rpss{{\hbox{rad\ts s\sup{-2}}}\SP}
\def\statcpcc{{\hbox{statcoulomb\ts cm\sup{-3}}}\SP}
\def\statcpsqc{{\hbox{statcoulomb\ts cm\sup{-2}}}\SP}
\def\V{{\hbox{Volts}}\SP}

%       ------------------------------------------
%       USEFUL MATH SYMBOLS SPECIFIC TO THIS PAPER
%       ------------------------------------------

\def\mdot{{\ifmmode \dot M \else {$\dot M$}\fi}}
\def\mdote{{\ifmmode \dot M_E \else {$\dot M_E$}\fi}}
\def\mdoti{{\ifmmode \dot M_i \else {$\dot M_i$}\fi}}
\def\msun{{\ifmmode M_\odot \else {$M_{\odot}$}\fi}}
\def\nonrot{{\rm 0}}


%                           ------------------
%                           SET TEX PARAMETERS
%                           ------------------

% SET TEX PARAMETERS FOR THIS PAPER
%
\magnification=1100
%
%\hoffset=0truein\hsize=0truein
%\voffset=0truein\vsize=0truein
%
\vsize=9 truein
\hsize=6.5 truein
%
\hyphenpenalty=10000
\hbadness=2000
\pretolerance=10000
\tolerance=5000
\raggedbottom
\baselineskip=12pt
\parindent=20pt
\parskip=0pt
%\hfuzz=14pt\overfullrule=0pt
\hfuzz=12pt\overfullrule=0pt
