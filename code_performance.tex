\mytitle{Code Performance and Scaling}

\setcounter{section}{0}
We must perform a large number of production runs to better understand
the dynamics of each physical scenario we study.  Given the large
computational cost per simulation, making each production run faster
is essential to achieving our scientific goals. Therefore, improving
code performance and scalability constitutes an ongoing effort of our
group.

Prior to adding adaptive-mesh refinement (AMR) capabilities to our
code, a typical compact object binary simulation involving black holes
and/or neutron stars required between 320--512 nodes per production
simulation. We now need only around 32 nodes for a comparable-resolution
production simulation.  According to the AMR weak scaling test presented in
Figure~\ref{abescalingtest}, we could increase this number to $64$
(resulting in a $\sim 26\%$ resolution improvement) and still maintain
better than $82\%$ scalability, as compared to a low-resolution, 4 node
evolution.  Making each production simulation faster is essential for
our science, since the physical properties of the objects we study are
neither well-understood nor fall within a small parameter space. 
%
%%%%%%%%%%%%%%%%%%%%%
%%%%    Figure   %%%%
%%%%%%%%%%%%%%%%%%%%%

\begin{figure}
  \begin{center}
\includegraphics[width=0.75\textwidth,angle=-90]{scale}
%\includegraphics[width=0.45\textwidth]{plot-openmponvsoff}
    \caption{ Performance of the GRMHD Illinois code on the TACC {\tt Stampede2}
     cluster. System simulated is a single stable neutron star using 4th-order
     finite differencing and 4 ghost zones. Both the
     spacetime and fluid are evolved.
    {\bf Left panel}: Scalability tests. Absolute performance is measured in iterations per
     second, keeping the number of spatial gridpoints per processor
     core fixed (weak scaling).  We normalize this performance to that
     on 4 nodes (``relative performance'').  The grid hierarchy
     consists of 8 nested refinement levels, using the Carpet code~4.0 AMR~\cite{web:Carpet}.
     Our highest resolution production runs to date use between $\approx~24$ to $48$ nodes,
     where we observe  $\approx 90\%$ of perfect scaling  (to within measurement error) scaling
     from 4 nodes with our code. Beyond our typical highest resolutions, we find $\approx$82\%
     of scaling at 64 nodes. Here we use $4$ MPI processes per node, , with 17 OpenMP
     threads per MPI process.
     {\bf Right panel}:
     MPI-only vs.~OpenMP/MPI hybrid code benchmark. Simulation is
     performed on $16$  nodes, varying the number of MPI processes per node.
     Relative performance is measured with respect to the
     1-MPI-process-per-node, OpenMP-disabled case.  For the OpenMP/MPI
     hybrid tests, we set the {\tt OMP\_NUM\_THREADS} parameter (which
    specifies the number of threads per MPI process) to the integer $N/M$, or the
    closet one, where $N$ is the total number of processor cores per node,
    and $M$ is the total number of MPI processes per node.  For MPI-only, we set
    {\tt OMP\_NUM\_THREADS=1}.  All these tests were performed in the {\tt tacc\_affinity}
    environment.}
    \label{abescalingtest}
  \end{center}
\end{figure}
%
Despite its great advantages, there is one important caveat when using
AMR grids for multi-node runs, as compared to a large uniform
resolution grid with the same effective resolution.  MPI
parallelization breaks up the grids into pieces and distributes them
among the processors, adding ``ghost zones'' to the boundaries of each
grid (since a value of a variable depends on values of other variables
at nearby grid points).  This ghost zone information must be supplied
to nearby MPI processes over the network.  As the grid is broken into
smaller and smaller pieces, the ratio of ``ghost zones'' to interior
zones increases, and, correspondingly, the network 
requirements for perfect (strong) scaling increase.  As a result, a
large uniform resolution grid will exhibit strong scaling to many
more processors than an AMR grid, with its multiple, relatively tiny
refinement grids.  To combat this network-communication-limited
performance, we have added full OpenMP support to our code.

Our implementation of OpenMP does not require any network communication, yet parallelizes
our loops across all available processor cores on each node.  With OpenMP enabled, we are able to
reduce the number of MPI processes run per node on XSEDE supercomputers, such as {\tt Stampede2} or
{\tt Comet} (which has been already used for production runs; see~e.g.~\cite{Khan017}),
and thus reduce the overall network communication. The net impact of OpenMP on our code's performance
on {\tt Stampede2} as a function of the number of MPI processes per node is shown in Figure~\ref{abescalingtest}.
The results demonstrate that the best performance with OpenMP enabled (4 MPI processes per node,
with 17 OpenMP threads per MPI process) is about 35\% better than the best performance with OpenMP
disabled  on 16 nodes.
We show the scaling of our code with respect to {\tt Stampede2}, where we have all our residual remaining
XSEDE resources, but we are targeting {\tt Comet} for many of our XSEDE runs, as discussed above. As we have
mentioned before, in our experience our code performance is better than on BW, e.g.  our code is a
factor of $\sim 2.2$ faster than on BW using the same number of nodes.


In terms of single-processor performance, each variable we evolve in
our GRMHD evolution code depends at a specific grid point on many other
variable values at nearby grid points (around 150 in total) each time it
is updated.  Due to this strong nonlinearity in the equations, cache
misses have the largest impact on single-processor code performance.
As a result, our code is usually memory-bound on a single processor.
Recently, we rewrote the main loops of our GRMHD code, reading in each needed
variable from main memory at the beginning of the loop, thus
minimizing cache misses. The net result was a 250\% improvement in
single processor performance.

