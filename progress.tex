\mytitle{Progress Report}

Over the past years, our group has made significant progress on a
number of computational  projects involving the physics of compact
objects, including compact binaries and their roles in MA. 
Many of these projects were completed using
XSEDE allocations, which have been acknowledged in the
resulting publications (see the document ``Publications Resulting from
XSEDE Support'').

\setcounter{section}{0}


%%%%%%%%%%%%%%%%%%%%%%%
%%%  BHNS  project %%%%
%%%%%%%%%%%%%%%%%%%%%%%

\section{Black Hole--Neutron Star Binary Mergers}
\label{prog_bhe}
\vspace{-0.2cm}
Recently,  we performed fully general relativistic BHNS
simulations~\cite{Paschalidis:2014} in which the NS is initially
endowed with a dipole magnetic field extending from the stellar
interior into its exterior. These dipole magnetic fields are 
more realistic, crudely mimicking pulsar-like magnetospheres, and enable
poloidal magnetic fields to thread the BH horizon prior to merger. Our
preliminary evolutions of these systems demonstrate for the first
time in full GRMHD that BHNS binaries can launch jets about $100(M_{\rm
NS}/1.4M_\odot)$ms following merger. Moreover, the lifetime of the
remnant disk, which provides the jet fuel, is $\sim 0.5(M_{\rm NS}/
1.4M_\odot)$ms, which is consistent with typical durations of sGRBs.
The EM luminosity is also consistent with typical sGRB observations. 
As a crucial step in establishing BHNS systems as viable progenitors of central engines that power
sGRBs and thereby solidify their role as multimessenger sources, we recently survey different BHNS
configurations that differ in the spin of the BH companion ($a/M_{\rm BH}=-0.5,\,0,\,0.5,\,0.75$),
in the mass ratio ($q=3:1$ and $q=5:1$), and in the orientation of the B-field (aligned and tilted
by $90^\circ$ with respect to the orbital angular momentum). We find in~\cite{Ruiz:2018wah} that
by $\Delta t\sim 3500M\sim 88(M_{\rm NS}/1.4M_\odot)\rm ms$ after the peak gravitational wave
signal a magnetically--driven jet is launched in the cases where the initial spin of the BH
companion is $a/M_{\rm BH}= 0.5$.  The lifetime of the jets [$\Delta t\sim 0.7(M_{\rm NS}/
1.4M_\odot)\rm s$] and their outgoing Poynting luminosities [$L_{jet}\sim
10^{52}\rm erg/s$] are
consistent with typical sGRBs, as well as with the Blandford--Znajek mechanism  for launching jets
and their associated Poynting luminosities. By the time we terminate our simulations,  we do not
observe either an outflow or a large-scale B-field collimation in the other
configurations simulated.
These results suggest that there must be a threshold value of the tilt-angle B-fields for jet
launching.

%These results
%create the urgent need to probe these configurations further in order
%to corroborate the jet identification and establish BHNSs both as sGRB
%engines and multimessenger sources. 


%%%%%%%%%%%%%%%%%%%%%%%
%%%  NSNS  project %%%%
%%%%%%%%%%%%%%%%%%%%%%%
\section{Binary Neutron Star Mergers }
\label{prog_bns}
\vspace{-0.2cm}
During the last two years we advanced our studies of  binary neutron stars
in three different scenarios:

\begin{enumerate}
\item We studied the development and saturation of the $m=1$ one-armed spiral
instability in  unmagnetized simulation of merging binary neutron
stars~\cite{East:2015vix,Paschalidis:2015mla,East:2016zvv}. We focused on the particular
case of a dynamical capture merger where the stars have a small spin, as
may arise in globular clusters. We found that this instability develops when
post-merger fluid vortices lead to the generation of a toroidal remnant.
The instability quickly saturates on a timescale of $\sim 10$ms.
Gravitational waves emitted by the $m=1$ instability have a peak frequency around
$1-2\ \mathrm{kHz}$ and, if detected, could be used to constrain the EOS
of neutron stars. In a recent paper~\cite{East:2016zvv}, we provided estimates
of the properties of dynamical ejecta, as well as the accompanying kilonovae signatures.

\item Motivated by our recent discovery that BHNSs can launch jets if the neutron
star is initially endowed with a dipole B-field that extends from
the NS interior to its exterior, we simulated magnetized NSNS systems with a
dynamically weak B-field, with the above configuration as well as when it
is confined to the NS interior~\cite{Ruiz:2016rai}. We found that, in contrast with
the BHNS systems, the NSNS systems launch jets both for interior-only and for
interior plus exterior dipole magnetic field configurations. 
The key ingredient for
generating a jet was found to be the ability of the remnant 
HMNS to amplify the magnetic field prior to its \textit{delayed} collapse to a BH.
These results spark our urgent need to probe
new configurations to corroborate the robustness of this outcome and to establish
more firmly NSNSs as candidates for sGRB engines.

\item We studied the possibility of jet formation from BH accretion disk
remnants which originate from more massive NSNS mergers that undergo {\it prompt} collapse. We found that
after $t-t_{\rm BH}\sim 26(M_{\rm NS}/1.8M_\odot)$ms [$M_{\rm NS}$ is the ADM mass] following
prompt BH formation, there is no evidence of collimated mass outflow or B-field collimation.
The rapid formation of the BH following merger prevents magnetic energy growth from approaching force-free
values above the magnetic poles, which is required for the launching of a jet by the usual
Blandford--Znajek mechanism. So, we expect that detection of GWs in coincidence with sGRBs may
provide constraints on the nuclear EOS: the fate of an NSNS merger--delayed
or prompt collapse, and hence the appearance or nonappearance of an sGRB--depends on a critical
value of the total mass of the binary, and the threshold mass above which prompt collapse occurs
is sensitive to the EOS.

We have  combined the coincident detections of gravitational waves with electromagnetic signals
from event GW170817/GRB170817A and our previous numerical simulations  to constrain the nature of
its progenitor~\cite{Ruiz:2017due}. This conclusion leads to a bound on the maximum mass of a cold,
spherical neutron star (the TOV limit): ${M_{\rm max}^{\rm sph}}\leq2.74/\beta$, where $\beta$ is
the ratio of the maximum mass of a uniformly rotating neutron star (the supramassive limit)
over the maximum mass of a nonrotating star. Causality arguments allow $\beta$
to be as high as $1.27$, while most realistic candidate EOS
predict $\beta$ to be closer to $1.2$, yielding ${M_{\rm max}^{\rm sph}}$ in
the range $2.16-2.28 M_\odot$.

\item  Using population synthesis studies along with the prompt collapse simulations we recently
showed in~\cite{Paschalidis:2018tsa} that  a non-repeating, precursor fast-radio burst may
be the most likely electromagnetic counterpart of these binaries.  The outgoing EM burst in these
cases is rather isotropic,  making the detection of coincident FRB and GW signatures possible.

\item For isentropic fluids, dynamical evolution of a binary system conserves the         
baryonic mass and circulation; therefore, sequences of constant rest mass and       
constant circulation are of particular importance. In \cite{Tsokaros:2018dqs} we  
presented the extension of our COCAL code to compute such quasiequilibria and       
compared them with the well-known corotating and irrotational sequences, the        
latter being the simplest, zero-circulation case. To assess the different        
measures of spin, we first computed         
sequences of single, uniformly rotating stars and described how the different       
spin diagnostics are related to each other. The connection to spinning binary       
systems was accomplished through the concept of circulation and the use of the   
constant rotational velocity formulation \cite{Tichy:2012rp}. Finally, we        
proposed a modification of the latter formulation that naturally leads to        
differentially rotating binary systems.   
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%
%%%  BHBHS  + disk %%%%
%%%%%%%%%%%%%%%%%%%%%%%

\section{Binary Black Hole Mergers in Gaseous Environments}
\label{bbh_prog}
\vspace{-0.2cm}

We have made significant progress on this problem with our simulations
of equal-mass BHBH mergers in the presence of magnetized gas:
\begin{enumerate}
\item We studied the influence
of the binary mass ratio \cite{gpesp14} on the quasistationary state 
that the disk achieves during the predecoupling epoch, finding that 
the accretion rate, and hence the expected emergent radiation depends 
on the mass ratio, although it still remains comparable to the 
accretion rate onto a single BH with the same total mass as the binary.
\item  We studied the effects of the mass ratio during the post-decoupling 
and merger phases~\cite{gprsep14}. These simulations showed that the 
density lump at the disk inner edge diminishes as the mass ratio 
decreases. We also made the important prediction that even a past 
merger event could be identified based on studies of jet morphology. 

\item We have initiated a study of the effect of the accretion disk thickness onto
non-spinning  stellar BHBHs with mass ratio $q=36/29$ (event GW150914).
We consider three initial disk models that differ in their scale
heights, physical extent and in their magnetic field content~\cite{Khan017}.
We found that such systems could explain possible gravitational wave and
electromagnetic counterparts such as the Fermi GBM hard X-ray signal
reported 0.4s after GW150915 ended~\cite{FermiGBM_GW150914}. 

\item Recently, we presented a new initial data formulation to solve the \textit{full set} of 
Einstein equations for spacetimes that contain a BH under general conditions~\cite{Tsokaros:2018zlf}.
We used that formulation to solve for the first time for a non-axisymmetric, self-gravitating 
torus in the presence of a black hole whose dimensionless spin was $J_{\rm bh}/M_{\rm bh}^2=0.9918$, 
with a rotation axis  tilted at  a $30^\circ$ angle with respect to the angular momentum of 
the disk, and  the mass of $\sim 1/5$ of the disk.
\end{enumerate}


\section{Stability and Collapse of Rotating Stars}
\label{prog_sms}

We have recently performed GRMHD simultions on SMSs collapse by evolving 
in full 3D uniformly rotating $\Gamma = 4/3$, magnetized polytropes
that are marginally stable. They model the direct collapse of supermassive stars (SMSs) to
seed BHs that can become supermassive BHs, as well as the collapse of massive Pop III stars,
which could power  long gamma-ray bursts (GRBs). Our preliminary results show that the
stars collapse  to a massive and highly spinning BH remnant surrounded by a  hot,
magnetized torus. By $t \approx 2000(M/10^6M_\odot)$s following the GW peak amplitude,
an incipient jet is launched. The disk lifetime and the outgoing Poynting
luminosity are consistent with those expected from the Blandford--Znajek  process.
If $\sim 1\%$  of the luminosity is converted into gamma-rays, then \textit{Swift} could
potentially detect these events at large redshifts~\cite{Sun:2017voo}.

We extended our previous calculations by considering SMSs described by  $\Gamma \gtrsim 4/3$,
polytropes to account for the perturbative role of gas pressure in SMSs with $M \lesssim
10^{6}M_\odot$. We also consider different initial stellar rotation profiles.
We find that the mass of the black hole remnant is $90\%-99\%$ of the initial stellar mass,
depending sharply on $\Gamma -4/3$ as well as on the initial stellar rotation profile. After
$t\sim 250-550M\approx 1-2\times 10^3(M/10^6M_\odot)$s following the appearance of the BH
horizon, an incipient jet is launched and it lasts for $\sim 10^4-10^5(M/10^6M_\odot)$s,
consistent with the duration of long gamma-ray bursts. Our numerical results suggest that
the Blandford-Znajek mechanism powers the incipient jet.

We have  also explored channels for BHBH formation. In particular, it has been suggested that
fragmentation of highly differentially rotating massive stars that undergo collapse
has been suggested as a viable BHBH formation channel. We recently probe that scenario
using GRMHD simulation of {\it stable} differentially rotating massive stars supported by thermal
radiation pressure domination plus a gas pressure perturbation~\cite{Sun:2018gcl}.
We find that magnetic braking and turbulent viscous damping  in the bulk of the star
redistribute  angular momentum, damp differential rotation  and induce the formation
of a massive and nearly uniformly rotating inner core surrounded by a Keplerian envelope
which evolves on a secular timescale and remains in quasi-stationary equilibrium until
the termination of our simulations. These results suggest that the high degree of
differential rotation required for $m=2$ seed density perturbations to trigger
gas fragmentation and BH formation is likely to be suppressed during the normal
lifetime of the star prior to evolving to the point of dynamical instability to collapse.
Our results suggest that other cataclysmic events, such as stellar mergers leading to collapse,
may therefore be necessary to reestablish sufficient differential rotation and density perturbations
to drive nonaxisymmetric modes leading to BHBH formation.
\section{Uniformly rotating triaxial neutron stars}
In \cite{Uryu:2016pto} we computed for the first time
triaxial \textit{supramassive} NSs (uniformly rotating
models with rest-mass higher than the maximum rest-mass of a
non-rotating star, but lower than the maximum rest-mass when
allowing for maximal uniform rotation), by using a piecewise
polytropic EOS. In our preliminary work \cite{Tsokaros:2017ueb} 
we performed the first evolutions of such
stars and investigated their stability and GW content. 
We were able to follow the evolution of these objects for more than
twenty rotation periods, proving that they are \textit{dynamically
stable}. After an initial short period of time where junk radiation
in the initial data propagates away, the NS evolves along
quasi-equilibrium states that satisfy the first law, $dM=\Omega
dJ$. Along this trajectory the orbital angular velocity remains
constant inside the NS, whose triaxial shape evolves toward
axisymmetry.  During this period the GW amplitude decreases
significantly, especially in the highly compact models. Also we showed
that the peak GW amplitude is approximately one tenth of that of 
binary neutron stars.



%%%%%%%%%%%%%%%%%%%%%
%%%  GRMHD Code  %%%%
%%%%%%%%%%%%%%%%%%%%%

\section{General Relativistic Magnetohydrodynamics Code Development}
\label{prog_mhd}
The current version of the Illinois GRMHD code solves the magnetic induction equation
by introducing a magnetic vector potential, which guarantees that
the $B$-field remains divergence-free for any
interpolation scheme used on refinement level boundaries in AMR~\cite{els10}.
This formulation reduces to the standard, CT scheme on uniform grids.
We introduced an improved Lorenz gauge condition for
evolving the vector potential in
AMR without the appearance of spurious $B$-fields on
refinement level boundaries~\cite{epls2012}.
This gauge exhibits no
zero-speed modes, enabling spurious magnetic effects to propagate off
the grid quickly. Our technique has been employed by
others \cite{Giacomazzo:2013uua,2014arXiv1410.0013G}). 


Recently, we extended our code to solve the equations of force-free
electrodynamics matched to regions exhibiting more general MHD 
behavior~\cite{PS13}.  We used this module in our first-ever study 
of NS magnetospheres around BHs~\cite{Paschalidis:2013jsaxo} and in 
the first-ever GR force-free simulations of the pulsar spin-down 
luminosity \cite{Ruiz:2014zta}.

In addition, we have re-written, optimized and made our GRMHD code
open-source~\cite{Etienne2015} and ported it into the publicly
available Einstein Toolkit~\cite{web:einsteintoolkit}. We plan to
continue the development of our robust, open-source AMR GRMHD code and
make it the default choice in the Einstein Toolkit. Our new code scales to
twice the number of nodes as our old code does, and the new code is
approximately two times faster than our old code. We will gradually
port our all of our code's diagnostics and tools into the Einstein
toolkit.


%%%%%%%%%%%%%%%%%%%%%%%
%%%  Visualization %%%%
%%%%%%%%%%%%%%%%%%%%%%%

\section{Visualization}
\label{prog_vis}
\vspace{-0.2cm}
Our computations produce large amounts of data in 3 dimensions
plus time; these data often can only be analyzed with sophisticated
visualization tools. We use open source software like VisIt to
analyze data from our code interactively. In addition, we train
an REU team of 4-6 undergraduates to create visualization tools
especially adopted for our simulations. Training of the REU students
in computational physics and visualization depends crucially on their
working with data generated by our supercomputer simulations.
Performing these simulations depends critically on the XSEDE resources
we receive. The time and space resolutions required for adequate
visualization and the resulting diagnosis of the underlying physics
helps set the scale of our request.

Our REU team has worked with us to produce movies that provide important physical and 
astrophysical insights into  our simulations. These have
included videos of colliding BHs; the stability of rotating
neutron stars against bar formation;
the collapse of magnetized neutron stars to black holes;
magnetic braking and viscous damping in differentially rotating stars;
the collapse of magnetized HMNSs, magnetorotational collapse
of massive stellar cores; and BHBH, NSNS and BHNS binary mergers, including
GW150914 (see
{\tt http://tinyurl.com/shapiromovies}).
In collaboration with UIUC
Professor Donna J. Cox and her staff we and our REU team recently have
embarked on generating the first 3D visualizations of GR compact
object binary mergers (using 3D glasses!). These provide unprecedented views and
insights into our simulation data. We will also work
with Professor Donna Cox and members of her group as part of the NSF CADENS
(Centrality of Advanced Digitally ENabled Science) project to develop
ultra-high resolution documentaries for museums, planetariums and science
centers. 



